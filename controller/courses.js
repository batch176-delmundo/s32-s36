const Course = require('../models/Course');


//Create a new course
/*
Steps:
	1. Create a new Course object using the monggoose model and the informtation from the request body.
	2. Save the new Course to the database
*/
module.exports.addCourse = (reqBody) => {

	//create a variable "newCourse" and instantiate the name, description, price
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price
	});

	//Save the created object to our database
	return newCourse.save().then((course, error) => {
		//Course creation is successful or not
		if (error) {
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}


//Retrieve all courses
//1. Retrieve all the courses from the database.
module.exports.getAllCourses =() => {
	return Course.find({}).then(result => {
		return result;
	})
}

//Retrieve all ACTIVE courses
//1. Retrieve all the courses with the property isActive: true

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	}).catch(error => error)
}

//Retrieve a specific course
//1. Retrieeve the course that matches the course ID provided from the URL

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result =>{
		return result
	}).catch(error => error)
}


//UPDATE a course
/*
Steps:
1. Create a variable "updatedCourse" which will contain the info retrieved from the req.body
2. Find and Update the course using the courseId retrieved from the req.params and the variable "updatedCourse" containing info from req.body

*/
module.exports.updateCourse = (courseId, data) => {

	//specify the fields/properties of the documents to be updated
	let updatedCourse = {
		name: data.name,
		description: data.description,
		price: data.price
	}
	//findByIdAndUpdate (document Id, updatedToBeApplied)
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}


//Archiving a course
//1. update the status of "isActive" into "false" which will no longer be displayed in the client whenever all active courses are retrieved.
module.exports.archiveCourse =(courseId) => {
	let updateActiveField = {
		isActive: false
	};
	return Course.findByIdAndUpdate(courseId, updateActiveField).then((course, error) =>{
		if (error) {
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}



